var player;
var keyboard;
var platforms = [];

var leftWall;
var rightWall;
var ceiling;

var text1;
var text2;
var text3;
var distance = 0;
var status = 'running';
var scream = new Audio("tools/Screem.mp3");
var walk= new Audio("tools/walk.mp3");
var slipper= new Audio("tools/slipper.mp3");
var shock=new Audio('tools/shock.mp3');
var jump=new Audio('tools/jump.mp3');
var lastTime = 0;
var gamestate={
    preload :function () {
        game.load.crossOrigin = 'anonymous';
        game.load.spritesheet('player', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/player.png', 32, 32);
        game.load.image('wall', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/wall.png');
        game.load.image('ceiling', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/ceiling.png');
        game.load.image('normal', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/normal.png');
        game.load.image('nails', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/nails.png');
        game.load.spritesheet('conveyorRight', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/fake.png', 96, 36);
    },

    create :function () {

        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D,
            'esc':Phaser.Keyboard.ESC
        });
        this.createBounders();
        this.createPlayer();
        this.createTextsBoard();
        game.input.keyboard.addKey(Phaser.Keyboard.ESC).onDown.add(this.back, this);
    },

    update :function  () {

        // bad
        if(status == 'gameOver' && keyboard.enter.isDown) this.restart();
        if(status != 'running') return;

        this.physics.arcade.collide(player, platforms, effect);
        this.physics.arcade.collide(player, [leftWall, rightWall]);
        checkTouchCeiling(player);
        this.checkGameOver();

        this.updatePlayer();
        this.updatePlatforms();
        this.updateTextsBoard();

        this.createPlatforms();
    },

    createBounders :function  () {
        leftWall = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(leftWall);
        leftWall.body.immovable = true;
        leftWall.scale.setTo(1,1.5);
        rightWall = game.add.sprite(483, 0, 'wall');
        game.physics.arcade.enable(rightWall);
        rightWall.body.immovable = true;
        rightWall.scale.setTo(1,1.5);
        ceiling = game.add.image(0, 0, 'ceiling');
        ceiling.scale.setTo(1.5,1);
    },

    
    createPlatforms :function  () {
        if(game.time.now > lastTime + 600) {
            lastTime = game.time.now;
            this.createOnePlatform();
            distance += 1;
        }
    },

    createOnePlatform:function  () {

        var platform;
        var x = Math.random()*(500 - 96 - 40) + 20;
        var y = 600;
        var rand = Math.random() * 100;

        if(rand < 20) {
            platform = game.add.sprite(x, y, 'normal');
        } else if (rand < 40) {
            platform = game.add.sprite(x, y, 'nails');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);
        } else if (rand < 50) {
            platform = game.add.sprite(x, y, 'conveyorLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 60) {
            platform = game.add.sprite(x, y, 'conveyorRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 80) {
            platform = game.add.sprite(x, y, 'trampoline');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        } else {
            platform = game.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }

        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        platforms.push(platform);

        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
    },

    createPlayer:function () {
        player = game.add.sprite(250, 50, 'player');
        player.direction = 10;
        game.physics.arcade.enable(player);
        player.body.gravity.y = 500;
        player.animations.add('left', [0, 1, 2, 3], 8);
        player.animations.add('right', [9, 10, 11, 12], 8);
        player.animations.add('flyleft', [18, 19, 20, 21], 12);
        player.animations.add('flyright', [27, 28, 29, 30], 12);
        player.animations.add('fly', [36, 37, 38, 39], 12);
        player.life = 10;
        player.unbeatableTime = 0;
        player.touchOn = undefined;
    },

    createTextsBoard:function  () {
        var style = {fill: '#BBFF66', fontSize: '30px'}
        var anotherstyle={fill:'#770077',fontSize:'25px'}
        text1 = game.add.text(30, 10, '', style);
        text2 = game.add.text(350, 10, '', style);
        text3 = game.add.text(game.width/2-100, 300, 'Enter to restart', anotherstyle);
        text3.visible = false;
    },

    updatePlayer:function  () {
        if(keyboard.left.isDown) {
            player.body.velocity.x = -250;
        } else if(keyboard.right.isDown) {
            player.body.velocity.x = 250;
        } else {
            player.body.velocity.x = 0;
        }
        setPlayerAnimate(player);
    },


    updatePlatforms: function () {
        for(var i=0; i<platforms.length; i++) {
            var platform = platforms[i];
            platform.body.position.y -= 2;
            if(platform.body.position.y <= -20) {
                platform.destroy();
                platforms.splice(i, 1);
            }
        }
    },

    updateTextsBoard:function  () {
        text1.setText('blood:' + player.life);
        text2.setText('floor:' + distance);
    },
    
    checkGameOver:function  () {
        if(player.life <= 0 || player.body.y > 700) {
            this.gameOver();
        }
    },

    gameOver :function () {
        writedata();
        text3.visible = true;
        platforms.forEach(function(s) {s.destroy()});
        platforms = [];
        status = 'gameOver';
    },

    restart:function  () {
        text3.visible = false;
        distance = 0;
        this.createPlayer();
        status = 'running';
    },
    back:function(){
        bgmusic.stop();
        //this.restart();
        game.state.start('menu');
    }
}
function setPlayerAnimate(player) {
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;

    if (x < 0 && y > 0) {
        player.animations.play('flyleft');
    }
    if (x > 0 && y > 0) {
        player.animations.play('flyright');
    }
    if (x < 0 && y == 0) {
        player.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player.animations.play('right');
    }
    if (x == 0 && y != 0) {
        player.animations.play('fly');
    }
    if (x == 0 && y == 0) {
    player.frame = 8;
    }
}
function effect(player, platform) {
    if(platform.key == 'conveyorRight') {
        conveyorRightEffect(player, platform);
        slipper.play();
    }
    if(platform.key == 'conveyorLeft') {
        conveyorLeftEffect(player, platform);
        slipper.play();
    }
    if(platform.key == 'trampoline') {
        trampolineEffect(player, platform);
        jump.play();
    }
    if(platform.key == 'nails') {
        nailsEffect(player, platform);
        
    }
    if(platform.key == 'normal') {
        basicEffect(player, platform);
    }
    if(platform.key == 'fake') {
        fakeEffect(player, platform);
        shock.play();
    }
}

function conveyorRightEffect(player, platform) {
    player.body.x += 2;
}

function conveyorLeftEffect(player, platform) {
    player.body.x -= 2;
}

function trampolineEffect(player, platform) {
    platform.animations.play('jump');
    player.body.velocity.y = -350;
}

function nailsEffect(player, platform) {
    if (player.touchOn !== platform) {
        player.life -= 3;
        scream.play();
        player.touchOn = platform;
        game.camera.flash(0xff0000, 100);
    }
}

function basicEffect(player, platform) {
    if (player.touchOn !== platform) {
        walk.play();
        if(player.life < 10) {
            player.life += 1;
        }
        player.touchOn = platform;
    }
}

function fakeEffect(player, platform) {
    if(player.touchOn !== platform) {
        platform.animations.play('turn');
        setTimeout(function() {
            platform.body.checkCollision.up = false;
        }, 100);
        player.touchOn = platform;
    }
}

function checkTouchCeiling(player) {
    if(player.body.y < 0) {
        if(player.body.velocity.y < 0) {
            player.body.velocity.y = 0;
        }
        if(game.time.now > player.unbeatableTime) {
            player.life -= 3;
            scream.play();
            game.camera.flash(0xff0000, 100);
            player.unbeatableTime = game.time.now + 2000;
        }
    }
}
function writedata(){
    var Renew = new Date();
    var username = (firebase.auth().currentUser.displayName != null) ? firebase.auth().currentUser.displayName : prompt("Please Enter Your Name: ", "");
    //如果是創立假帳號的話 就利用prompt來讓使用者輸入他自己的名稱 不然就用firebase得知她的displayname
    firebase.database().ref('Record').push().set({//record表示的是他的節點名稱 底下的東西是我們想要得知的資訊最後把它push到firebase上
        username: username,
        data: distance,
        Year: Renew.getFullYear(),
        Month: Renew.getMonth() + 1,
        Day: Renew.getDate(),
        Hours: Renew.getHours(),
        Minutes: Renew.getMinutes(),
        Seconds: Renew.getSeconds()
    });
}
