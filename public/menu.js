var play_one_guy;
var play_two_guy;
var leaderboard;
var selectmenu=1;
var coverphoto;
var bgmusic;

var style = {fill: '#660077', fontSize: '30px'}
var anotherstyle={fill:'#ffffff',fontSize:'30px'}
var menustate = {
    // Define all the functions of the state
    // This function will be executed at the beginning
    preload: function() { 
        game.load.audio('bgmusic','tools/bgmusic.mp3');
    },
    // This function is called after the preload function
    create: function() {
        play_one_guy=game.add.text(game.width/2-70,game.height/2-100,'play one',style);
        play_two_guy=game.add.text(game.width/2-70,game.height/2,'play two',anotherstyle);
        leaderboard=game.add.text(game.width/2-70,game.height/2+100,'leaderboard',anotherstyle);
        game.input.keyboard.addKey(Phaser.Keyboard.UP).onDown.add(this.prechoice, this);
        game.input.keyboard.addKey(Phaser.Keyboard.DOWN).onDown.add(this.nextchoice, this);
        game.input.keyboard.addKey(Phaser.Keyboard.W).onDown.add(this.prechoice, this);
        game.input.keyboard.addKey(Phaser.Keyboard.S).onDown.add(this.nextchoice, this);
        game.input.keyboard.addKey(Phaser.Keyboard.ENTER).onDown.add(this.start, this);
        //coverphoto=game.add.image(0,0,'bg');
        getrank();
        bgmusic=game.add.audio('bgmusic');
        bgmusic.play();
     },
     
    // This function is called 60 times per second
    update: function() { 

    },
    // And maybe add some other functions
    nextchoice:function(){
        if(selectmenu==1){
            selectmenu=2;
            play_one_guy=game.add.text(game.width/2-70,game.height/2-100,'play one',anotherstyle);
            play_two_guy=game.add.text(game.width/2-70,game.height/2,'play two',style);
            leaderboard=game.add.text(game.width/2-70,game.height/2+100,'leaderboard',anotherstyle);
        }
        else if(selectmenu==2){
            selectmenu=3;
            play_one_guy=game.add.text(game.width/2-70,game.height/2-100,'play one',anotherstyle);
            play_two_guy=game.add.text(game.width/2-70,game.height/2,'play two',anotherstyle);
            leaderboard=game.add.text(game.width/2-70,game.height/2+100,'leaderboard',style);
        }
        else if(selectmenu==3){
            selectmenu=1;
            play_one_guy=game.add.text(game.width/2-70,game.height/2-100,'play one',style);
            play_two_guy=game.add.text(game.width/2-70,game.height/2,'play two',anotherstyle);
            leaderboard=game.add.text(game.width/2-70,game.height/2+100,'leaderboard',anotherstyle);
        }
    },
    prechoice :function(){
        if(selectmenu==1){
            selectmenu=3;
            play_one_guy=game.add.text(game.width/2-70,game.height/2-100,'play one',anotherstyle);
            play_two_guy=game.add.text(game.width/2-70,game.height/2,'play two',anotherstyle);
            leaderboard=game.add.text(game.width/2-70,game.height/2+100,'leaderboard',style);
        }
        else if(selectmenu==2){
            selectmenu=1;
            play_one_guy=game.add.text(game.width/2-70,game.height/2-100,'play one',style);
            play_two_guy=game.add.text(game.width/2-70,game.height/2,'play two',anotherstyle);
            leaderboard=game.add.text(game.width/2-70,game.height/2+100,'leaderboard',anotherstyle);
        }
        else if(selectmenu==3){
            selectmenu=2;
            play_one_guy=game.add.text(game.width/2-70,game.height/2-100,'play one',anotherstyle);
            play_two_guy=game.add.text(game.width/2-70,game.height/2,'play two',style);
            leaderboard=game.add.text(game.width/2-70,game.height/2+100,'leaderboard',anotherstyle);
        }
    },
    start:function(){
        if(selectmenu==1){
            distance = 0;
            gamestate.createPlayer();
            status = 'running';
            platforms.forEach(function(s) {s.destroy()});
            platforms = [];
            game.state.start('game');
        }
        else if(selectmenu==2){
            distance = 0;
            gametwostate.createPlayer();
            gametwostate.createPlayer2();
            status = 'running';
            platforms.forEach(function(s) {s.destroy()});
            platforms = [];
            game.state.start('game2');
        }
        else if(selectmenu==3){
            game.state.start('leaderboard');
        }
    }

};
function getrank() {
    var postsRef = firebase.database().ref('Record');
    var total_post = [];
    var name_post = [];
    var time_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                var name = childSnapshot.val().username;
                var bestrecord = childSnapshot.val().data;
                var year = childSnapshot.val().Year;
                var month = childSnapshot.val().Month;
                var day = childSnapshot.val().Day;
                var hours = childSnapshot.val().Hours;
                var minutes = childSnapshot.val().Minutes;
                var seconds = childSnapshot.val().Seconds;
                if (hours < 10)
                    hours = '0' + hours;
                if (minutes < 10)
                    minutes = '0' + minutes;
                if (seconds < 10)
                    seconds = '0' + seconds;
                total_post[total_post.length] = bestrecord;
                name_post[name_post.length] = name;
                time_post[time_post.length] = year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds;
                first_count += 1;
            });

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var name = data.val().username;
                    var bestrecord = data.val().data;
                    var year = data.val().Year;
                    var month = data.val().Month;
                    var day = data.val().Day;
                    var hours = data.val().Hours;
                    var minutes = data.val().Minutes;
                    var seconds = data.val().Seconds;
                    if (hours < 10)
                        hours = '0' + hours;
                    if (minutes < 10)
                        minutes = '0' + minutes;
                    if (seconds < 10)
                        seconds = '0' + seconds;
                    total_post[total_post.length] = bestrecord;
                    name_post[name_post.length] = name;
                    time_post[time_post.length] = year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds;
                }
            });
            for (var i = total_post.length - 1; i > 0; i--) {
                for (var j = 0; j < i; j++) {
                    if (total_post[j] < total_post[j + 1]) {
                        var tmp, tmp_name, tmp_time;

                        tmp = total_post[j];
                        total_post[j] = total_post[j + 1];
                        total_post[j + 1] = tmp;

                        tmp_name = name_post[j];
                        name_post[j] = name_post[j + 1];
                        name_post[j + 1] = tmp_name;

                        tmp_time = time_post[j];
                        time_post[j] = time_post[j + 1];
                        time_post[j + 1] = tmp_time;
                    }
                }
            }
            firstplace_name = name_post[0];
            firstplace_record = total_post[0];
            firstplace_time = time_post[0];
            secondplace_name = name_post[1];
            secondplace_record = total_post[1];
            secondplace_time = time_post[1];
            thirdplace_name = name_post[2];
            thirdplace_record = total_post[2];
            thirdplace_time = time_post[2];
            number_of_users = total_post.length;
            console.log('lenght: ' + total_post.length);
            console.log('1.' + firstplace_name + ':' + firstplace_record + '->' + firstplace_time);
            console.log('2.' + secondplace_name + ':' + secondplace_record + '->' + secondplace_time);
            console.log('3.' + thirdplace_name + ':' + thirdplace_record + '->' + thirdplace_time);
        })
        .catch(e => console.log(e.message));
}

    