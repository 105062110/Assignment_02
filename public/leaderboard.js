var t1;
var t2;
var t3;
var t4;
var t5;
var t6;
var t7;
var t8;
var t9;
var leaderstate={
    preload :function(){},
    create:function(){
        var astyle = {fill: '#FFFFFF', fontSize: '20px'}
        var cstyle={fill:'#FFFFFF',fontSize:'10px'}
        t1 = game.add.text(0, 100, "1st "+firstplace_name+" Record:"+firstplace_record, astyle);
        t3 = game.add.text(300, 150, firstplace_time, cstyle);
        t4 = game.add.text(0, 200,"2nd "+secondplace_name+" Record:"+secondplace_record , astyle);
        t6 = game.add.text(300, 250, secondplace_time, cstyle);
        t7 = game.add.text(0, 300,"3rd "+thirdplace_name+" Record:"+thirdplace_record , astyle);
        t9 = game.add.text(300, 350,thirdplace_time , cstyle);
        game.input.keyboard.addKey(Phaser.Keyboard.ESC).onDown.add(this.back, this);
    },
    update:function(){},
    back:function(){
        bgmusic.stop();
        game.state.start('menu');
    }
}
