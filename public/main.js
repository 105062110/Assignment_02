var game = new Phaser.Game(500, 600, Phaser.AUTO, 'canvas');
var firstname,
    secondname,
    thirdname,
    firsttime,
    secondtime,
    thirdtime,
    firstrecord,
    secondrecord,
    thirdrecord;
game.global = { 
    firstname,
    secondname,
    thirdname,
    firsttime,
    secondtime,
    thirdtime,
    firstrecord,
    secondrecord,
    thirdrecord
}; 
game.state.add('menu',menustate);
game.state.add('game',gamestate);
game.state.add('game2',gametwostate);
game.state.add('leaderboard',leaderstate);
game.state.start('menu');
